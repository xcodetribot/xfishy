package scripts.xFishy.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.tribot.api.General;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.types.RSItem;

import scripts.xFishy.xFishy;

public enum FishType {
	LEAPING_FISH 		(new String[] {"Leaping trout", "Leaping salmon", "Leaping sturgeon"}, new String[] {"Barbarian rod"}, new String[] {"Feather", "Fishing bait"}, new String[] {"Use-rod"})
	{
		@Override
		public final String toString()
		{
			return "Leaping fish";
		}
	},
	LOBSTERS 			(new String[] {"Raw lobster"}, new String[] {"Lobster pot"}, new String[]{}, new String[] {"Cage", "Harpoon"}),
	SHARKS 				(new String[] {"Raw shark"}, new String[] {"Harpoon"}, new String[]{}, new String[] {"Harpoon", "Net"}),
	SHRIMPS_ANCHOVIES 	(new String[] {"Raw shrimps", "Raw anchovies"}, new String[] {"Small fishing net"}, new String[]{}, new String[] {"Net", "Bait"}),
	TROUT_SALMON 		(new String[] {"Raw trout", "Raw salmon"}, new String[] {"Fly fishing rod"}, new String[] {"Feather"}, new String[] {"Lure", "Bait"}),
	TUNA_SWORDFISH 		(new String[] {"Raw tuna", "Raw swordfish"}, new String[] {"Harpoon"}, new String[]{}, new String[] {"Harpoon", "Cage"});

	private String[] fish;
	private String[] primaryGear; // Harpoon, Cage, Net, etc.
	private String[] secondaryGear; // Feather, Bait, etc.
	
	//The fishing pool that must contain certain actions like: Bait and Net. The 0-index must be the one you use to fish.
	private String[] spotContains;
	
	FishType(String[] fishName, String[] primGear, String[] secGear, String[] spotContains)
	{
		this.fish = fishName;
		this.primaryGear = primGear;
		this.secondaryGear = secGear;
		this.spotContains = spotContains;
	}
	
	public String[] getFish()
	{
		return fish;
	}
	
	public void setFish(String[] name)
	{
		this.fish = name;
	}
	
	public boolean requiredGear()
	{
		RSItem[] primaryItems = Inventory.find(primaryGear);
		RSItem[] secondaryItems = Inventory.find(secondaryGear);
		RSItem[] requiredLocationItems = new RSItem[0];
		int requiredLocationItemsLength = 0;
		if(xFishy.location != null && xFishy.location.getNecessaryItems().length > 0)
		{
			requiredLocationItems = Inventory.find(xFishy.location.getNecessaryItems());
		}
		if((primaryGear.length > 0 && primaryItems.length == 0) ||
				(secondaryGear != null && secondaryGear.length > 0 && secondaryItems.length == 0) || 
				(requiredLocationItemsLength > 0 && requiredLocationItems.length == 0))
			return false;
		else
			return true;
	}
	
	public String[] getGear()
	{
		ArrayList<String> gear = new ArrayList<String>();
		
		for(String s : primaryGear)
			gear.add(s);
		
		for(String s : secondaryGear)
			gear.add(s);
		
		if(xFishy.location != null)
			for(String s : xFishy.location.getNecessaryItems())
				gear.add(s);
		
		String[] allGear = new String[gear.size()];
		gear.toArray(allGear);
		return allGear;
	}
	
	public String[] getPrimaryGear()
	{
		return this.primaryGear;
	}
	
	public String[] getSecondaryGear()
	{
		return this.secondaryGear;
	}
	
	public void setPrimaryGear(String[] name)
	{
		this.primaryGear = name;
	}
	
	public void setSecondaryGear(String[] name)
	{
		this.secondaryGear = name;
	}
	
	public String[] getSpotContains()
	{
		return spotContains;
	}
	
	public void setSpotContains(String[] contains)
	{
		this.spotContains = contains;
	}
	
	@Override
	public String toString()
	{
		String s = "";
		for(int i = 0 ; i < fish.length ; i++)
		{
			String[] se = fish[i].split("Raw ");
			s += Character.toUpperCase(se[1].charAt(0)) + se[1].substring(1);
			if(i != fish.length - 1)
				s+= " / ";
		}
		
		return s;
	}
}
