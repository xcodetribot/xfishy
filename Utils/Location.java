package scripts.xFishy.Utils;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Camera;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.NPCChat;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Objects;
import org.tribot.api2007.PathFinding;
import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSTile;

import scripts.xFishy.xFishy;

public enum Location {
	BAXTORIAN_FALLS	 	("Baxtorian Falls", new RSArea(new RSTile(2508, 3486, 0), new RSTile(2497, 3514, 0)), null, new RSTile(2498, 3508, 0), new FishType[] {FishType.LEAPING_FISH}),
	BARBARIAN_VILLAGE 	("Barbarian Village", new RSArea(new RSTile(3095, 3420, 0), new RSTile(3115,3440, 0)), new RSTile(3093, 3491, 0), new RSTile(3104, 3429, 0), new FishType[] {FishType.TROUT_SALMON}),
	CATHERBY	 		("Catherby", new RSArea(new RSTile(2834, 3435, 0), new RSTile(2862, 3420, 0)), new RSTile(2809, 3441, 0), new RSTile(2843, 3432, 0), new FishType[] {FishType.SHARKS, FishType.LOBSTERS, FishType.TUNA_SWORDFISH, FishType.SHRIMPS_ANCHOVIES}),
	DRAYNOR 			("Draynor", new RSArea(new RSTile(3076, 3241, 0), new RSTile(3090,3222, 0)), new RSTile(3092, 3243, 0), new RSTile(3087, 3230, 0), new FishType[] {FishType.SHRIMPS_ANCHOVIES}),
	FISHING_GUILD 		("Fishing Guild", new RSArea(new RSTile(2594, 3418, 0), new RSTile(2606, 3427, 0)), new RSTile(2586, 3420, 0), new RSTile(2600, 3423, 0), new FishType[] {FishType.SHARKS, FishType.LOBSTERS, FishType.TUNA_SWORDFISH}),
	LUMBRIDGE_SWAMP 	("Lumbridge Swamp", new RSArea(new RSTile(3246, 3160, 0), new RSTile(3234,3135, 0)), null, new RSTile(3243, 3152, 0), new FishType[] {FishType.SHRIMPS_ANCHOVIES}),
	SEERS_VILLAGE 		("Seers Village", new RSArea(new RSTile(2735, 3522, 0), new RSTile(2710, 3540, 0)), new RSTile(2726, 3492, 0), new RSTile(2726, 3527, 0), new FishType[] {FishType.TROUT_SALMON}),
	KARAMJA 			("Karamja", new RSArea(new RSTile(2912, 3185, 0), new RSTile(2928,3172, 0)), new RSTile(3046, 3235, 0), new RSTile(2924, 3179, 0), new FishType[] {FishType.SHRIMPS_ANCHOVIES, FishType.LOBSTERS, FishType.TUNA_SWORDFISH}, new String[]{"Coins"}, true)
	{
		final RSTile seersBoardingPosition = new RSTile(3028, 3217, 0);
		final RSTile depositBoxPosition = new RSTile(3045, 3235, 0);
		final RSTile seersBoatTile = new RSTile(3032, 3217, 1);
		final RSTile karamjaBoatTile = new RSTile(2956, 3143, 1);
		final RSTile karamjaBoardingTile = new RSTile(2955, 3148, 0);
		
		@Override
		public void toBank()
		{
			RSItem[] coins = Inventory.find("Coins");
			if(coins.length == 0 || (coins.length > 0 && coins[0].getStack() < 30))
			{
				xFishy.stop = true;
				General.println("Not enough money for boat. Stopping script.");
			}
			
			RSNPC[] sailor = NPCs.findNearest("Customs officer");
			
			if((sailor.length == 0 || (sailor.length > 0 && !sailor[0].isOnScreen())) && seersBoardingPosition.distanceTo(Player.getPosition()) > 6 && Objects.findNearest(10, "Bank deposit box").length == 0)
			{
				PathFinding.aStarWalk(karamjaBoardingTile);
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						return NPCs.findNearest("Customs officer").length > 0;
					}
				}, General.random(4000, 5000));
			}
			
			sailor = NPCs.findNearest("Customs officer");
			
			if(sailor.length > 0 && sailor[0].isOnScreen())
			{
				if(DynamicClicking.clickRSNPC(sailor[0], "Pay-Fare"))
				{
					Timing.waitCondition(new Condition() {
						
						@Override
						public boolean active() {
							return NPCChat.getClickContinueInterface() != null;
						}
					}, General.random(3000, 4000));
					
						if(NPCChat.getClickContinueInterface() != null && NPCChat.getMessage().contains("I help you?"))
						{
							NPCChat.clickContinue(true);
						}
						
						if(NPCChat.getSelectOptionInterface() != null && NPCChat.getOptions().length > 0 && NPCChat.getOptions()[0].contains("I journey on this ship"))
						{
							NPCChat.selectOption("Can I journey on this ship?", true);
						}
						if(NPCChat.getClickContinueInterface() != null && NPCChat.getMessage().contains("I journey on this ship?"))
						{
							NPCChat.clickContinue(true);
						}
						if(NPCChat.getClickContinueInterface() != null && NPCChat.getMessage().contains("to be searched before you"))
						{
							NPCChat.clickContinue(true);
						}
						if(NPCChat.getSelectOptionInterface() != null && NPCChat.getOptions().length > 1 && NPCChat.getOptions()[1].contains("Search away, I have nothing to hide."))
						{
							NPCChat.selectOption("Search away, I have nothing to hide.", true);
						}
						if(NPCChat.getClickContinueInterface() != null && NPCChat.getMessage().contains("Search away, I have nothing to hide."))
						{
							NPCChat.clickContinue(true);
						}
						if(NPCChat.getClickContinueInterface() != null && NPCChat.getMessage().contains("Well you've got some odd stuff"))
						{
							NPCChat.clickContinue(true);
						}
						if(NPCChat.getSelectOptionInterface() != null && NPCChat.getOptions().length > 0 && NPCChat.getOptions()[0].contains("Ok."))
						{
							NPCChat.selectOption("Ok.", true);
						}
						if(NPCChat.getClickContinueInterface() != null && NPCChat.getMessage().contains("Ok."))
						{
							if(NPCChat.clickContinue(true))		
							{
								Timing.waitCondition(new Condition() {
									
									@Override
									public boolean active() {
										return Player.getPosition().distanceTo(seersBoatTile) == 0 && Inventory.getAll().length > 0;
									}
								}, General.random(7000, 8000));
							}
						}
				}
			}
				
			if(Player.getPosition().getPlane() == 1)
			{
				RSObject[] gankplank = Objects.findNearest(3, "Gangplank");
				if(gankplank.length > 0)
				{
					if(!gankplank[0].isOnScreen())
						Camera.turnToTile(gankplank[0].getPosition());
					
					if(DynamicClicking.clickRSObject(gankplank[0], "Cross"))
					{
						Timing.waitCondition(new Condition() {
							
							@Override
							public boolean active() {
								return NPCs.findNearest("Seaman Thresnor", "Seaman Tobias", "Seaman Lorris").length > 0;
							}
					}, General.random(3000, 4000));
					}
				}
			}
			if(NPCs.findNearest("Seaman Thresnor", "Seaman Tobias", "Seaman Lorris").length > 0 && depositBoxPosition.distanceTo(Player.getPosition()) >= 5 && Inventory.isFull())
			{
				PathFinding.aStarWalk(depositBoxPosition);
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						RSObject[] depositBox = Objects.findNearest(5, "Bank deposit box");
						return depositBox.length > 0 && depositBox[0].isOnScreen();
					}
				}, General.random(12000, 15000));
			}
		}
		
		@Override
		public void toFishingSpot()
		{
			if(super.fishingSpotPosition.distanceTo(Player.getPosition()) <= 6)
				return;
			
			RSItem[] coins = Inventory.find("Coins");
			if(coins.length == 0 || (coins.length > 0 && coins[0].getStack() < 30))
			{
				xFishy.stop = true;
				General.println("Not enough money for boat. Stopping script.");
			}
			
			RSNPC[] sailors = NPCs.findNearest("Seaman Thresnor", "Seaman Tobias", "Seaman Lorris");
			
			if((sailors.length == 0 || (sailors.length > 0 && !sailors[0].isOnScreen())) && seersBoardingPosition.distanceTo(Player.getPosition()) > 10)
			{
				PathFinding.aStarWalk(seersBoardingPosition);
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						RSNPC[] sailors = NPCs.findNearest("Seaman Thresnor", "Seaman Tobias", "Seaman Lorris");
						return sailors.length > 0 && sailors[0].isOnScreen();
					}
				}, General.random(6000, 7000));
			}
			
			sailors = NPCs.findNearest("Seaman Thresnor", "Seaman Tobias", "Seaman Lorris");
			
			if(sailors.length > 0 && sailors[0].isOnScreen())
			{
				if(DynamicClicking.clickRSNPC(sailors[0], "Pay-fare"))
				{
					Timing.waitCondition(new Condition() {
						
						@Override
						public boolean active() {
							return NPCChat.getClickContinueInterface() != null;
						}
					}, General.random(3000, 4000));

					if(NPCChat.getClickContinueInterface() != null && NPCChat.getMessage().contains("will cost you 30 coins"))
					{
						NPCChat.clickContinue(true);
					}
					
					if(NPCChat.getSelectOptionInterface() != null && NPCChat.getOptions().length > 0 && NPCChat.getOptions()[0].contains("Yes please."))
					{
						NPCChat.selectOption("Yes please.", true);
					}
					if(NPCChat.getClickContinueInterface() != null && NPCChat.getMessage().contains("Yes please"))
					{
						if(NPCChat.clickContinue(true))
						{
							Timing.waitCondition(new Condition() {
								
								@Override
								public boolean active() {
									return Player.getPosition().distanceTo(karamjaBoatTile) == 0 && Inventory.getAll().length > 0;
								}
							}, General.random(7000, 8000));
						}
					}
				}
			}
			if(Player.getPosition().getPlane() == 1)
			{
				RSObject[] gankplank = Objects.findNearest(3, "Gangplank");
				if(gankplank.length > 0)
				{
					if(!gankplank[0].isOnScreen())
						Camera.turnToTile(gankplank[0].getPosition());
					
					if(DynamicClicking.clickRSObject(gankplank[0], "Cross"))
					{
						Timing.waitCondition(new Condition() {
							
							@Override
							public boolean active() {
								return NPCs.findNearest("Customs officer").length > 0;
							}
						}, General.random(3000, 4000));
					}
				}
			}
			
			if(NPCs.findNearest("Customs officer").length > 0)
			{
				WebWalking.walkTo(super.fishingSpotPosition, new Condition() {
					
					@Override
					public boolean active() {
						RSNPC[] fishingSpots = NPCs.findNearest("Fishing spot");
						return fishingSpots.length > 0 && fishingSpots[0].isOnScreen();
					}
				}, General.random(20000, 22000));
			}
		}
	};
	
	private String name;
	private RSArea area; // Only spots where to fish
	private String[] necessaryItems;
	private RSTile bankPosition;
	private FishType[] fishTypes;
	private RSTile fishingSpotPosition;
	private boolean depositBox;
	
	Location(String name, RSArea locationArea, RSTile bankPosition, RSTile fishingSpotPosition, FishType[] fishTypes)
	{
		this.name = name;
		this.area = locationArea;
		this.bankPosition = bankPosition;
		this.fishingSpotPosition = fishingSpotPosition;
		this.fishTypes = fishTypes;
		this.necessaryItems = new String[]{};
		this.depositBox = false;
	}
	
	Location(String name, RSArea locationArea, RSTile bankPosition, RSTile fishingSpotPosition, FishType[] fishTypes, String[] necItems, boolean depositBox)
	{
		this.name = name;
		this.area = locationArea;
		this.bankPosition = bankPosition;
		this.fishingSpotPosition = fishingSpotPosition;
		this.fishTypes = fishTypes;
		this.necessaryItems = necItems;
		this.depositBox = depositBox;
	}
	
	public RSArea getArea()
	{
		return this.area;
	}
	
	public void setArea(RSArea area)
	{
		this.area = area;
	}
	
	public String[] getNecessaryItems()
	{
		return this.necessaryItems;
	}
	
	public void setNecessaryItems(String[] items)
	{
		this.necessaryItems = items;
	}
	
	public RSTile getBankingSupported()
	{
		return this.bankPosition;
	}
	
	public void setBankingSupported(RSTile bankPos)
	{
		this.bankPosition = bankPos;
	}
	
	public RSTile getFishingSpotPosition()
	{
		return this.fishingSpotPosition;
	}
	
	public void setFishingSpotPosition(RSTile pos)
	{
		this.fishingSpotPosition = pos;
	}
	
	public FishType[] getFishTypes()
	{
		return this.fishTypes;
	}
	
	public void setFishTypes(FishType[] fishes)
	{
		this.fishTypes = fishes;
	}
	
	public boolean isAtLocation(RSTile tile)
	{
		return area.contains(tile);
	}
	
	public boolean isUsingDepositBox()
	{
		return depositBox;
	}
	
	@Override
	public String toString()
	{
		return name;
	}
	
	public void toFishingSpot()
	{
		
		WebWalking.walkTo(fishingSpotPosition, new Condition() {
			
			@Override
			public boolean active() {
				return fishingSpotPosition.distanceTo(Player.getPosition()) <= 5;
			}
		}, General.random(3000, 4000));
	}
	
	public void toBank()
	{
		if(bankPosition != null)
		{
			WebWalking.walkTo(bankPosition, new Condition() {
				
				@Override
				public boolean active() {
					return bankPosition.distanceTo(Player.getPosition()) <= 3;
				}
			}, General.random(3000, 4000));
		}
		else
		{
			General.println("No banking supported for this location. Stopping script.");
			xFishy.stop = true;
		}
	}
}
