package scripts.xFishy.Nodes;

import org.tribot.api2007.Game;
import org.tribot.api2007.Options;
import org.tribot.api2007.Player;
import org.tribot.api2007.Skills.SKILLS;

import scripts.xFishy.xFishy;
import scripts.xFishy.Utils.Node;

public class Antiban extends Node{

	@Override
	public boolean isValid() {
		return Player.getAnimation() != -1 || Player.isMoving();
	}

	@Override
	public void execute() {
		xFishy.AntiBan.performCombatCheck();

		xFishy.AntiBan.performEquipmentCheck();

		xFishy.AntiBan.performExamineObject();

		xFishy.AntiBan.performFriendsCheck();

		xFishy.AntiBan.performLeaveGame();

		xFishy.AntiBan.performMusicCheck();

		xFishy.AntiBan.performPickupMouse();

		xFishy.AntiBan.performQuestsCheck();

		xFishy.AntiBan.performRandomMouseMovement();

		xFishy.AntiBan.performRandomRightClick();

		xFishy.AntiBan.performRotateCamera();

		xFishy.AntiBan.performTimedActions(SKILLS.FISHING);

		xFishy.AntiBan.performXPCheck(SKILLS.FISHING);
		
		if(Player.isMoving() && Game.getRunEnergy() >= xFishy.AntiBan.INT_TRACKER.NEXT_RUN_AT.next())
		{
			Options.setRunOn(true);
			xFishy.AntiBan.INT_TRACKER.NEXT_RUN_AT.reset();
		}
		
	}

}
