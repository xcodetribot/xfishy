package scripts.xFishy.Nodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Inventory.DROPPING_PATTERN;
import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;

import scripts.xFishy.*;
import scripts.xFishy.Utils.FishingMethods;
import scripts.xFishy.Utils.Node;

public class InventoryFull extends Node{

	@Override
	public boolean isValid() {
		return  
				(Inventory.isFull() ||
						xFishy.fishingMethod == FishingMethods.F1D1);
	}

	@Override
	public void execute() {
		switch(xFishy.fishingMethod)
		{
			case BANKING:
				bank();
				break;
			case DROPPING:
				drop();
				break;
			case F1D1:
				f1d1();
				break;
			default:
				drop();
				break;
		}
		
	}
	
	private void bank()
	{
		xFishy.location.toBank();
	}
	
	private void drop()
	{
		Inventory.dropAllExcept(xFishy.fishType.getGear());
	}
	
	private void f1d1()
	{
		if(Inventory.isFull())
			Inventory.dropAllExcept(xFishy.fishType.getGear());
		if(Inventory.getCount(xFishy.fishType.getFish()) > 0)
		{
				Inventory.drop(xFishy.fishType.getFish());
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						return Inventory.getCount(xFishy.fishType.getFish()) == 0;
					}
				}, General.random(500, 600));
		}
		
	}

}
