package scripts.xFishy.Utils;

public enum FishingMethods {
	DROPPING ("Dropping"), 
	F1D1 ("F1D1"), 
	BANKING ("Banking");
	
	private String name;

	FishingMethods(String name) {
		this.name = name;
	}
	
	@Override
	public String toString()
	{
		return name;
	}
}
