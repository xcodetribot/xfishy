package scripts.xFishy.Utils;

import org.tribot.api2007.PathFinding;
import org.tribot.api2007.types.RSTile;

public abstract class Node {
	
	/**
     * Is action valid?
     * @return 
     */
    public abstract boolean isValid();
    
    /**
     * Execute action.
     */
    public abstract void execute();
    
    public boolean stringArrayContains(String[] array, String str)
	{
		for(int i = 0 ; i < array.length ; i++)
			if(array[i].equalsIgnoreCase(str))
				return true;
		return false;
	}
    
    public boolean spotCanBeUsed(RSTile spot)
	{
		RSTile[] surroundingSpots = new RSTile[]
				{
					new RSTile(spot.getX() - 1, spot.getY()),
					new RSTile(spot.getX() + 1, spot.getY()),
					new RSTile(spot.getX(), spot.getY() - 1),
					new RSTile(spot.getX(), spot.getY() + 1),
				};
		
		for(int i = 0 ; i < surroundingSpots.length ; i++)
		{
			if(PathFinding.canReach(surroundingSpots[i], true) && PathFinding.isTileWalkable(surroundingSpots[i]))
				return true;
		}
		return false;
	}
}
