package scripts.xFishy.Nodes;

import java.awt.Point;
import java.awt.Rectangle;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.input.DynamicMouse;
import org.tribot.api.types.generic.Condition;
import org.tribot.api.types.generic.CustomRet_0P;
import org.tribot.api.types.generic.Filter;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Inventory.DROPPING_PATTERN;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.PathFinding;
import org.tribot.api2007.Player;
import org.tribot.api2007.Walking;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSTile;
import org.tribot.script.ScriptManifest;

import scripts.xFishy.xFishy;
import scripts.xFishy.Utils.*;

@ScriptManifest(authors = { "xCode" }, category = "Fishing", name = "xFishy")
public class UseFishingSpot extends Node{
	
	@Override
	public boolean isValid() {
		//RSNPC[] spots = NPCs.findNearest("Fishing spot");
		RSNPC[] spots = NPCs.findNearest(new Filter<RSNPC>()
				{
					@Override
					public boolean accept(RSNPC npc) {
						final String[] options = npc.getActions();
						if(xFishy.fishType.getSpotContains().length == 1)
							return options != null && stringArrayContains(options, xFishy.fishType.getSpotContains()[0]) && npc.getModel().getPoints().length < 50 && spotCanBeUsed(npc.getPosition()) && xFishy.location.getArea().contains(npc.getPosition());
						else
							return options != null && stringArrayContains(options, xFishy.fishType.getSpotContains()[0]) && xFishy.fishType.getSpotContains().length > 1 && stringArrayContains(options, xFishy.fishType.getSpotContains()[1]) && npc.getModel().getPoints().length < 50 && spotCanBeUsed(npc.getPosition()) && xFishy.location.getArea().contains(npc.getPosition());
					}
				});
		
		boolean spotNextToMe = false;
		for(int i = 0 ; i < spots.length ; i++)
		{
			if(spots[i].getPosition().distanceToDouble(Player.getPosition()) == 1.0)
			{
				spotNextToMe = true;
				break;
			}
		}
		
		return  spots.length > 0 && 
				Player.getAnimation() == -1 && 
				!Player.getRSPlayer().isInCombat() && !Inventory.isFull() &&
				xFishy.location != null &&
				xFishy.location.isAtLocation(Player.getPosition()) &&
				xFishy.fishType != null &&
				xFishy.fishType.requiredGear();
	}

	@Override
	public void execute() {
		//RSNPC[] spots = NPCs.findNearest("Fishing spot");
		
		RSNPC[] spots = NPCs.findNearest(new Filter<RSNPC>()
				{
					@Override
					public boolean accept(RSNPC npc) {
						final String[] options = npc.getActions();
						if(xFishy.fishType.getSpotContains().length == 1)
							return options != null && stringArrayContains(options, xFishy.fishType.getSpotContains()[0]) && npc.getModel().getPoints().length < 50 && spotCanBeUsed(npc.getPosition()) && xFishy.location.getArea().contains(npc.getPosition());
						else
							return options != null && stringArrayContains(options, xFishy.fishType.getSpotContains()[0]) && xFishy.fishType.getSpotContains().length > 1 && stringArrayContains(options, xFishy.fishType.getSpotContains()[1]) && npc.getModel().getPoints().length < 50 && spotCanBeUsed(npc.getPosition()) && xFishy.location.getArea().contains(npc.getPosition());
					}
				});
		if(spots.length == 0)
		{
			General.println("Walking back to fishing spot");
			xFishy.location.toFishingSpot();
			Timing.waitCondition(new Condition() {
				
				@Override
				public boolean active() {
					return !Player.isMoving() || NPCs.findNearest("Fishing spot").length > 0;
				}
			}, General.random(3000,4000));
			General.sleep(500,700);
			
		}
		
		for(int i = 0 ; i < spots.length ; i++)
		{
			final RSTile spotPos = spots[i].getPosition();
		//	final String[] options = spots[i].getActions();
		//	if(options == null || !stringArrayContains(options, xFishy.fishType.getSpotContains()[0]) || 
		//			(xFishy.fishType.getSpotContains().length > 1 && !stringArrayContains(options, xFishy.fishType.getSpotContains()[1])) ||
		//			spots[i].getModel().getPoints().length >= 50 || 
		//			!spotCanBeUsed(spotPos) || !xFishy.location.getArea().contains(spotPos))
		//		continue;
			
			if(xFishy.AntiBan.BOOL_TRACKER.USE_CLOSEST.next())
			{
				General.println("Skipped one");
				xFishy.AntiBan.BOOL_TRACKER.USE_CLOSEST.reset();
				continue;
			}
			
			if(!spots[i].isOnScreen())
			{
				Walking.blindWalkTo(spotPos, new Condition() {
					
					@Override
					public boolean active() {
						return spotPos.distanceTo(Player.getPosition()) <= 2;
					}
				}, General.random(5000, 6000));
				if(!spots[i].isOnScreen())
					continue;
			}
			if(DynamicClicking.clickRSNPC(spots[i], xFishy.fishType.getSpotContains()[0]))
			{
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						return Player.getAnimation() != -1 && !Player.isMoving();
					}
				}, General.random(5000,6000));
				
				if(xFishy.fishingMethod == FishingMethods.F1D1)
				{
					final Point p = getFirstEmptySlot();
					if(p != null)
						DynamicMouse.move(new CustomRet_0P<Point>() {
							
							@Override
							public Point ret() {
								return p;
						}
					});
				}
				break;
			}
			General.sleep(300, 600);
		}
	}
	
	private Point getFirstEmptySlot()
	{
		RSItem[] items = Inventory.getAll();
		int counter = 0;
		int firstEmptySlotIndex = -1;
		for(RSItem item : items)
		{
			if(item.getIndex() == counter)
			{
				counter++;
			}
			else
			{
				firstEmptySlotIndex = counter;
				break;
			}
		}
		
		if(firstEmptySlotIndex == -1)
			return null;
		
		RSItem r = new RSItem(firstEmptySlotIndex, -1, 1, RSItem.TYPE.INVENTORY); // Placeholder for the empty slot to get the area.
		Rectangle rec = r.getArea();
		
		return new Point(General.random((int)rec.getMinX(), (int)rec.getMaxX()), General.random((int)rec.getMinY(), (int)rec.getMaxY()));
	}

}
