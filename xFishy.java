package scripts.xFishy;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.util.ABCUtil;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Player;
import org.tribot.api2007.Inventory.DROPPING_PATTERN;
import org.tribot.api2007.Login;
import org.tribot.api2007.Login.STATE;
import org.tribot.api2007.Skills;
import org.tribot.api2007.Skills.SKILLS;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.EventBlockingOverride;
import org.tribot.script.interfaces.MessageListening07;
import org.tribot.script.interfaces.Painting;
import org.tribot.script.interfaces.EventBlockingOverride.OVERRIDE_RETURN;

import scripts.xFishy.Nodes.*;
import scripts.xFishy.Utils.FishType;
import scripts.xFishy.Utils.FishingMethods;
import scripts.xFishy.Utils.Location;
import scripts.xFishy.Utils.Node;

@ScriptManifest(authors = { "xCode" }, category = "Fishing", name = "xFishy", version = 1.5)
public class xFishy extends Script implements Painting, MessageListening07{

	public static final ABCUtil AntiBan = new ABCUtil();
	public static boolean stop = false;
	
	public static Location location;
	public static FishType fishType;
	public static FishingMethods fishingMethod;
	
	private boolean retrievedStats = false;
	private long startTime = 0;
	private int startingXP = 0;
	private int startingLevel = 0;
	private int fishesCaught = 0;
	private int currentXP = 0;
	private String status = "";
	
	private final Color transpBlackColor = new Color(0, 0, 0, 170);
	private final Color transpGreenColor = new Color(99, 209, 62, 170);
	private final Font textFont = new Font("Arial", 0, 13);
	private final Font textItalicFont = new Font("Arial", 2, 11);
	private final Font titleFont = new Font("Arial", 0, 18);
	
	private ArrayList<Node> nodes = new ArrayList<Node>();
	private final RenderingHints ANTIALIASING = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	
	@Override
	public void run() {
		initialize();
		
		GUI gui = new GUI();
		gui.setVisible(true);
		while(gui.isVisible())
		{
			General.sleep(100);
		}
		General.println(fishType.requiredGear());
		while(!stop)
		{
			
			
			for (final Node n : nodes) {
				if (n.isValid()) {
					status = n.getClass().getSimpleName();
					n.execute();
				}
			}
			sleep(General.random(10, 50));
		}
	}

	private void initialize()
	{
		Inventory.setDroppingPattern(DROPPING_PATTERN.TOP_TO_BOTTOM);
		
		if(Login.getLoginState() == STATE.INGAME && !retrievedStats)
		{
			startingLevel = Skills.getActualLevel(SKILLS.FISHING);
			startingXP = Skills.getXP(SKILLS.FISHING);
			currentXP = startingXP;
			startTime = Timing.currentTimeMillis();
			retrievedStats = true;
		}
		
		nodes.add(new DetectWhirlPool());
		nodes.add(new UseFishingSpot());
		nodes.add(new InventoryFull());
		nodes.add(new Antiban());
		nodes.add(new RetrieveEquipment());
		nodes.add(new BankFish());
		nodes.add(new WalkToLocation());
	}

	@Override
	public void onPaint(Graphics gg) {
		Graphics2D g = (Graphics2D) gg;

		if(Login.getLoginState() == STATE.INGAME)
		{
			currentXP = Skills.getXP(SKILLS.FISHING);
			String runTime = Timing.msToString((Timing.currentTimeMillis() - startTime));
			int gainedXP = currentXP - startingXP;
			int currentLevel = Skills.getActualLevel(SKILLS.FISHING);
			int gainedLevel = currentLevel - startingLevel;
			int percentToNextLevel = Skills.getPercentToNextLevel(SKILLS.FISHING);
			double xpH = ((double)gainedXP * (3600000.0 / (Timing.currentTimeMillis() - startTime)));
			long ttnl = (long)(Skills.getXPToNextLevel(SKILLS.FISHING) / xpH * 3600000);
			
			int paintX = 30;
			int paintY = 283;
			int row2offsetX = 290;
			
			g.setColor(transpBlackColor);
			g.fillRect(3, 246, 513, 93);
			
			g.setColor(Color.white);
			
			g.setFont(titleFont);
			g.drawString("xFishy", 235, 265);
			
			g.setFont(textItalicFont);
			g.drawString("by xCode", 235, 280);
			
			g.setFont(textFont);
			
			g.drawString("Runtime: " + runTime, paintX, paintY - 15);
			g.drawString("Fishes caught: " + fishesCaught + " [" + (int)((double)fishesCaught * (3600000.0 / (Timing.currentTimeMillis() - startTime))) + " P/H]", paintX, paintY);
			g.drawString("XP gained: " + gainedXP + " [" + (int)xpH + " P/H]", paintX, paintY + 15);
			g.drawString("Level: " + currentLevel + " [+" + gainedLevel + "]", paintX, paintY + 30);
			
			g.drawString("Method: " + fishingMethod.toString(), paintX + row2offsetX, paintY - 15);
			g.drawString("Location: " + location.toString(), paintX + row2offsetX, paintY);
			g.drawString("Fishing: " + fishType.toString(), paintX + row2offsetX, paintY + 15);
			g.drawString("Status: " + status, paintX + row2offsetX, paintY + 30);
			
			g.setColor(transpGreenColor);
			int greenBarWidth = (int) (513.0 * (percentToNextLevel / 100.0));
			g.fillRect(4, 322, greenBarWidth, 16);
			g.setColor(Color.DARK_GRAY);
			g.drawLine(4, 322, 517, 322);
			g.setColor(Color.WHITE);
			if(gainedXP > 0)
				g.drawString(percentToNextLevel + "% to " + (Skills.getActualLevel(SKILLS.FISHING) + 1) + " Fishing [" + Timing.msToString(ttnl) + "]", 155, 335);
		}
		
		
	}

	@Override
	public void clanMessageReceived(String arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void duelRequestReceived(String arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void personalMessageReceived(String arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void playerMessageReceived(String arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void serverMessageReceived(String message) {
		if (message.toLowerCase().contains("you catch")) 
            fishesCaught++;
	}

	@Override
	public void tradeRequestReceived(String arg0) {
		// TODO Auto-generated method stub
		
	}
}
