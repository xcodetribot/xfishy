package scripts.xFishy.Nodes;

import org.tribot.api2007.Inventory;
import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;

import scripts.xFishy.xFishy;
import scripts.xFishy.Utils.Node;

public class WalkToLocation extends Node{

	@Override
	public boolean isValid() {
		return 	xFishy.location != null &&
				!xFishy.location.isAtLocation(Player.getPosition()) && 
				!Inventory.isFull();
	}

	@Override
	public void execute() {
		xFishy.location.toFishingSpot();
	}

}
