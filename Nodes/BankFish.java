package scripts.xFishy.Nodes;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.rs3.widgets.EnterAmount;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Objects;
import org.tribot.api2007.types.RSObject;

import scripts.xFishy.xFishy;
import scripts.xFishy.Utils.FishingMethods;
import scripts.xFishy.Utils.Node;

public class BankFish extends Node{

	@Override
	public boolean isValid() {
		return (Banking.isInBank() || (xFishy.location != null && xFishy.location.isUsingDepositBox() && Objects.findNearest(5, "Bank deposit box").length > 0)) && xFishy.fishingMethod == FishingMethods.BANKING && Inventory.isFull();
	}

	@Override
	public void execute() {
		if(xFishy.location != null)
		{
			if(!xFishy.location.isUsingDepositBox())
			{
				if(!Banking.isBankScreenOpen())
				{
					Banking.openBank();
				}
				else
				{
					Banking.depositAllExcept(xFishy.fishType.getGear());
					if(!xFishy.fishType.requiredGear())
					{
						if(Inventory.find(xFishy.fishType.getPrimaryGear()).length < xFishy.fishType.getPrimaryGear().length)
						{
							if(!Banking.withdraw(1, xFishy.fishType.getPrimaryGear()))
							{
								General.println("We've lost the fishing gear and can't find it in bank. Stopping script.");
								xFishy.stop = true;
							}
						}
						if(Inventory.find(xFishy.fishType.getSecondaryGear()).length < xFishy.fishType.getSecondaryGear().length)
						{
							if(!Banking.withdraw(0, xFishy.fishType.getSecondaryGear()))
							{
								General.println("We've lost the fishing gear and can't find it in bank. Stopping script.");
								xFishy.stop = true;
							}
						}
					}
				}
			}
			else
			{
				RSObject[] boxes = Objects.findNearest(5, "Bank deposit box");
				if(!Banking.isDepositBoxOpen())
				{
					if(boxes.length > 0)
						if(DynamicClicking.clickRSObject(boxes[0], "Deposit"))
							Timing.waitCondition(new Condition() {
								
								@Override
								public boolean active() {
									return Banking.isDepositBoxOpen();
								}
							}, General.random(1000, 1500));
				}
				
				Banking.depositAllExcept(xFishy.fishType.getGear());
				
			}
		}
	}
	

}
