package scripts.xFishy.Nodes;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Player;
import org.tribot.api2007.Walking;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSTile;

import scripts.xFishy.xFishy;
import scripts.xFishy.Utils.Node;

public class DetectWhirlPool extends Node{

	@Override
	public boolean isValid() {
		RSNPC[] spots = NPCs.findNearest("Fishing spot");
		boolean whirlpool = false;
		for(int i = 0 ; i < spots.length ; i++)
		{
			final String[] options = spots[i].getActions();
			if(spots[i].getPosition().distanceToDouble(Player.getPosition()) == 1.0 && 
					stringArrayContains(options, xFishy.fishType.getSpotContains()[0]) && 
					((xFishy.fishType.getSpotContains().length > 1 && stringArrayContains(options, xFishy.fishType.getSpotContains()[1])) || (xFishy.fishType.getSpotContains().length == 1 && stringArrayContains(options, xFishy.fishType.getSpotContains()[0])))
					 && spots[i].getModel() != null &&
					spots[i].getModel().getPoints().length >= 50)
			{
					whirlpool = true;
					break;
			}
		}
		return  Player.getAnimation() != -1 && whirlpool;
				
				
	}

	@Override
	public void execute() {
		General.println("Whirlpool detected!");
		
		RSNPC[] spots = NPCs.findNearest("Fishing spot");
		for(int i = 0 ; i < spots.length ; i++)
		{
			RSTile spotPos = spots[i].getPosition();
			String[] options = spots[i].getActions();
			
			if(!stringArrayContains(options, xFishy.fishType.getSpotContains()[0]) || 
					((xFishy.fishType.getSpotContains().length > 1 && !stringArrayContains(options, xFishy.fishType.getSpotContains()[1])) || xFishy.fishType.getSpotContains().length == 1) ||
					spots[i].getModel().getPoints().length >= 50 || 
					!spotCanBeUsed(spotPos) || 
					spotPos.distanceToDouble(Player.getPosition()) <= 1.0)
				continue;
			
			if(DynamicClicking.clickRSNPC(spots[i], xFishy.fishType.getSpotContains()[0]))
			{
				General.sleep(800, 1500);
				return;
			}
		}
		Walking.blindWalkTo(xFishy.location.getFishingSpotPosition(), new Condition() {
			
			@Override
			public boolean active() {
				return !Player.isMoving();
			}
		}, General.random(1200,1400));
	}

}
