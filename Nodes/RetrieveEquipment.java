package scripts.xFishy.Nodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.GroundItems;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Player;
import org.tribot.api2007.Walking;
import org.tribot.api2007.types.RSGroundItem;
import org.tribot.api2007.types.RSTile;

import scripts.xFishy.xFishy;
import scripts.xFishy.Utils.Node;

public class RetrieveEquipment extends Node{

	@Override
	public boolean isValid() {
		return xFishy.fishType != null && !xFishy.fishType.requiredGear() && xFishy.location != null &&	xFishy.location.isAtLocation(Player.getPosition());
	}

	@Override
	public void execute() {
		General.sleep(3000,4000);
		final RSGroundItem[] gearOnGround = GroundItems.findNearest(xFishy.fishType.getGear());
		if(gearOnGround.length > 0)
		{
			General.println("Picking up our gear...");
			RSTile gearTile = gearOnGround[0].getPosition();
			if(gearTile.distanceTo(Player.getPosition()) > 3)
			{
				Walking.walkTo(gearTile);
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						return gearOnGround[0].isOnScreen();
					}
				}, General.random(3000,4000));
			}
			gearOnGround[0].click("Take");
			Timing.waitCondition(new Condition() {
				
				@Override
				public boolean active() {
					if(xFishy.fishType != null)
						return xFishy.fishType.requiredGear();
					else
						return false;
				}
			}, General.random(1000, 1500));
		}
		else
		{
			if(gearOnGround.length > 0 && xFishy.location.isAtLocation(Player.getPosition()))
			{
				General.println("We've lost the fishing gear and can't find it back. Stopping script.");
				xFishy.stop = true;
			}
		}
	}

}
