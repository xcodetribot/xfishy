package scripts.xFishy;

import java.awt.*;
import java.awt.event.*;
import java.util.Arrays;

import javax.swing.*;
import javax.swing.event.*;

import org.tribot.api.General;
import org.tribot.api2007.Login;
import org.tribot.api2007.Login.STATE;
import org.tribot.api2007.Player;

import scripts.xFishy.Utils.FishingMethods;
import scripts.xFishy.Utils.Location;

public class GUI extends JFrame {
    private JButton startButton;
    private JLabel jcomp2;
    private JComboBox locationBox;
    private JComboBox fishBox;
    private JLabel jcomp5;
    private JLabel jcomp6;
    private JComboBox methodBox;

    public GUI() {
        //construct components
        setTitle("xFishy GUI");
        startButton = new JButton ("Start fishing");
        jcomp2 = new JLabel ("Location:");
        locationBox = new JComboBox (Location.values());
        fishBox = new JComboBox (((Location) locationBox.getSelectedItem()).getFishTypes());
        jcomp5 = new JLabel ("Fish:");
        jcomp6 = new JLabel ("Fishing method: ");
        methodBox = new JComboBox (FishingMethods.values());
        Location.values();
        locationBox.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                changeFishes();
            }
        });
        
        startButton.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
            	xFishy.location = (Location) locationBox.getSelectedItem();
            	xFishy.fishType = ((Location) locationBox.getSelectedItem()).getFishTypes()[fishBox.getSelectedIndex()];
            	xFishy.fishingMethod = (FishingMethods) methodBox.getSelectedItem();
            	
            	if(xFishy.location.getBankingSupported() == null && xFishy.fishingMethod == FishingMethods.BANKING)
            	{
            		xFishy.stop = true;
            		General.println("Banking is not supported for " + xFishy.location.toString() + ". Stopping script.");
            	}
                setVisible(false);
            }
        });

        //adjust size and set layout
        setSize (new Dimension (280, 200));
        setLayout (null);

        //add components
        add (startButton);
        add (jcomp2);
        add (locationBox);
        add (fishBox);
        add (jcomp5);
        add (jcomp6);
        add (methodBox);

        //set component bounds (only needed by Absolute Positioning)
        startButton.setBounds (75, 125, 110, 20);
        jcomp2.setBounds (45, 10, 100, 25);
        locationBox.setBounds (110, 10, 130, 25);
        fishBox.setBounds (110, 40, 130, 25);
        jcomp5.setBounds (70, 40, 100, 25);
        jcomp6.setBounds (8, 70, 100, 25);
        methodBox.setBounds (110, 70, 130, 25);
    }


    private void changeFishes()
    {
    	fishBox.setModel(new JComboBox<>(((Location) locationBox.getSelectedItem()).getFishTypes()).getModel());
    	
    	this.invalidate();
    }
    
    public static void main (String[] args) {
        JFrame frame = new JFrame ("xFishy GUI");
        frame.setDefaultCloseOperation (JFrame.HIDE_ON_CLOSE);
        frame.getContentPane().add (new GUI());
        frame.pack();
        frame.setVisible (true);
    }
}
